package com.astontp.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@Tag("PersonneTest")
@DisplayName("Classe de test sur les noms et prenoms des personnes")
class PersonTest {
	
	Personne personneGlobal = new Personne(); // Créer une personne exemple pour mes tests
	String nomCompletGlobal; // Obtenir le nom complet de la personne

	@BeforeAll
	static void avantTousLesTests() throws Exception {
		System.out.println("Debut des tests");
	}

	@AfterAll
	static void finDeTousLesTests() throws Exception {
		System.out.println("Fin des tests");
	}

	@BeforeEach
	void avantChaqueTest() throws Exception {
		personneGlobal.setNom("TAHOBEU"); // Affectation du nom
		personneGlobal.setPrenom("Didier"); // Affectation du prénom
		nomCompletGlobal = personneGlobal.getNomComplet(); // Recupération du nom complet
	}

	@AfterEach
	void apresChaqueTest() throws Exception {
	}

	@Test
	@DisplayName("Test sur les noms")
	void validerNom() {
		
		assertEquals("TAHOBEU", personneGlobal.getNom());
	}
	
	@Test
	@DisplayName("Test sur les prénoms")
	void validerPrenom() {
		
		assertEquals("Didier", personneGlobal.getPrenom());
	}
	
	@Test
	@DisplayName("Test sur les noms complets")
	public void validerLeNomComplet() {
		
		assertEquals("Didier TAHOBEU", nomCompletGlobal);
	}
	
	@ParameterizedTest(name = "Prenom: {0}, Nom: {1}, Nom Complet: {2} ")
	@CsvSource({"Didier, TAHOBEU, Didier TAHOBEU", "John, Doe, John Doe", "Jane, Doe, Jane Doe", "Pierre, Dupont, Pierre Dupont"})
	@DisplayName("Test sur les noms complets paramétrés")
	public void testNomCompletAvecParametres(String prenom, String nom, String nomCompletAttendu) {
		// String nomCompletObtenu = prenom + " " + nom;
		Personne personne = new Personne();
		personne.setNom(nom);
		personne.setPrenom(prenom);
		
		assertEquals(nomCompletAttendu, personne.getNomComplet());
		
	}

}
